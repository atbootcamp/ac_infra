#!/bin/bash

# cd compiler project
cd Compiler_AT_Project

# install dependencies
pipenv shell
pipenv install

# database migrations
python manage.py makemigrations
python manage.py migrate

# runserver
python manage.py runserver 0.0.0.0:8080