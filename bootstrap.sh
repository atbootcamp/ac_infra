#!/bin/bash
# This script will download and start Apache, and then create a symlink between your synced files directory and the location where Apache will look for the content
apt-get update

# add python 3.9
apt install software-properties-common
add-apt-repository ppa:deadsnakes/ppa
apt -y install python3.9

# install pip3
apt-get -y install python3-pip

# install distutils library
apt-get install -y python3.9-distutils

# install pipenv
pip3 install pipenv
